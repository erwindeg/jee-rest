package nl.sogeti.demo.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import nl.sogeti.demo.model.Person;
import nl.sogeti.demo.service.PersonService;

@Path("/person")
@Produces(MediaType.APPLICATION_JSON)
public class PersonRestService {
	
	@Inject
	PersonService personService;
	
	@GET
	public List<Person> getPersons(){
		return this.personService.findAll();
	}
	
	@GET
	@Path("{id}")
	public Person getPerson(@PathParam("id") Long personID){
		return this.personService.find(personID);
	}
	
	@POST
	public Response createPerson(Person person){
		this.personService.persist(person);
		return Response.status(Status.OK).build();
	}

	
}
