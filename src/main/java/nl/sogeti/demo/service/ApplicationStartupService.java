package nl.sogeti.demo.service;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import nl.sogeti.demo.model.Person;

@Singleton
@Startup
public class ApplicationStartupService {
	@Inject
	PersonService personService;

	 @PostConstruct
	 public void init(){
		 Person person = new Person();
		 person.setName("naam");
		 this.personService.persist(person);
	 }
}
